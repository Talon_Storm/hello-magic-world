init python:
    ## Default items are marked with the index number 0.
    ##
    ## Current customizations editable by user:
    ## 1. Hairstyles
    ## 2. Hair color
    ##
    ## Other customization that's not editable by user:
    ## 1. Facial expressions
    ## 2. Clothes

    # this function draws the character sprites
    def draw_char(st,at,name,hair_col):
        return LiveComposite(
        (361,702),                           # 'finished' image size
        (0,0),"base.png",                    # (x,y) position of image, image to display
        (0,0),recolor("{0}hair.png".format(name),hair_col),
        (0,0),"{0}shirt.png".format(name),
        (0,0),"{0}pants.png".format(name)
        ),.1

    # this function draws the character portraits
    def draw_portrait(st,at,name,hair_col):
        return LiveComposite(
        (361,702),
        (10,500),im.Crop("base.png",(120,0,120,204)),
        (10,500),im.Crop(recolor("{0}hair.png".format(name),hair_col),(120,0,120,204)),
        (10,500),im.Crop("{0}shirt.png".format(name),(120,0,120,204)),
        (10,500),im.Crop("{0}pants.png".format(name),(120,0,120,204))
        ),.1

    # this function draws the BG with any other overlays
    def draw_bg(st,at):
        return LiveComposite(
        (1280,1024),
        (0,0),"bg.jpg",
        (0,0),"java.png"
        ),.1

    # this function recolors an image (0=red,1=green,2=blue)
    # more colors to be added, needs experimenting with the tint values
    # TODO use 3 sliders for each color channels and use the slider's value as tint's input
    def recolor(img,color):
        if color == 0:
            return im.MatrixColor(img,im.matrix.desaturate() * im.matrix.tint(1.0,0.0,0.0))
        elif color == 1:
            return im.MatrixColor(img,im.matrix.desaturate() * im.matrix.tint(0.0,1.0,0.0))
        elif color == 2:
            return im.MatrixColor(img,im.matrix.desaturate() * im.matrix.tint(0.0,0.0,1.0))
        else:
            return img

# script to define the characters and their image label
define aeolus = Character("Aeolus",image="aeolus")
define helios = Character("Helios",image="helios")

# script to define the characters' full image
image aeolus img = DynamicDisplayable(draw_char,"aeolus",0)
image helios img = DynamicDisplayable(draw_char,"helios",1)

# script to define the characters' side image (if needed)
image side aeolus = DynamicDisplayable(draw_portrait,"aeolus",0)
image side helios = DynamicDisplayable(draw_portrait,"helios",1)

# script to define the background images
image bg = DynamicDisplayable(draw_bg)

label start:
    # script to show background images (currently there aren't any)
    scene bg

    # script to play regular music (file format to use coming soon)
    play music "music/01-title.ogg"

    show aeolus img with dissolve

    # dialogue script
    aeolus "Hello, I am Aeolus!"

    # hiding and showing characters' full image
    hide aeolus img with dissolve
    show helios img with dissolve

    helios "And I am Helios!"

    hide helios img with dissolve

    # script to play sound effects (file format also coming soon)
    play sound "sound/e06-waves.mp3"

    "This is wave sounds playing in the background"

    return
