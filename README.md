# A game project with TBA title
Title to be announced.

## Project Description
A VN game project made using Ren'Py, currently WIP.

## Project Structure
The game project's root directory is `game` with the following tree as the structure:
```bash
game
    ├───cache
    ├───gui
    │   ├───bar
    │   ├───button
    │   ├───overlay
    │   ├───phone
    │   │   ├───bar
    │   │   ├───button
    │   │   ├───overlay
    │   │   ├───scrollbar
    │   │   └───slider
    │   ├───scrollbar
    │   └───slider
    ├───images
    ├───music
    ├───saves
    ├───sound
    └───tl
        └───None
```

Important directories to note:
- `images` : Directory for image assets.
- `music`  : Directory for music assets.
- `sound`  : Directory for sound effect assets.

## The Development Team
Story Writers:


Artists:


Music and Sound:


Programmers:
- DarkGreiga

## Project's TODO List
- [ ] A proper title.
- [ ] A neatly-formatted README document.
- [ ] An actual description.
- [ ] A separate screen for customization (probably needed).